package ru.smbatyan.bot.operators;

import jakarta.annotation.PostConstruct;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.smbatyan.bot.handlers.CommandHendler;

import java.util.ArrayList;
import java.util.List;


@Component
public class CommandsOperator {
    private final ConfigurableApplicationContext context;
    private final List<CommandHendler> listRegisteredHandlerCommands = new ArrayList<>();

    public CommandsOperator(ConfigurableApplicationContext context) {
        this.context = context;
    }

    @PostConstruct
    private void registerHandlerCommands() {
        context.getBeansOfType(CommandHendler.class).forEach((s, commandHendler) -> listRegisteredHandlerCommands.add(commandHendler));
    }

    public void request(Update update, TelegramLongPollingBot longPollingBot) {
        boolean flag = false;
        if (update.hasMessage() && update.getMessage().hasText()) {
            for (CommandHendler commandHendler : listRegisteredHandlerCommands) {
                for (String command : commandHendler.getListCommandName()) {
                    if (command.equals(update.getMessage().getText().trim())) {
                        commandHendler.processing(update, longPollingBot);
                        flag = true;
                    }
                }
            }
            if (!flag) {
                for (CommandHendler commandHendler : listRegisteredHandlerCommands) {
                    for (String command : commandHendler.getListCommandName()) {
                        if ("Default".equals(command)) {
                            commandHendler.processing(update, longPollingBot);
                        }
                    }
                }
            }
        }
    }
}
