package ru.smbatyan.bot.buttons;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.smbatyan.services.ProductService;

import java.util.ArrayList;
import java.util.List;

@Component
public class MenuButtons {

    private final ProductService productService;

    public MenuButtons(ProductService productService) {
        this.productService = productService;
    }

    public ReplyKeyboardMarkup getMenuButton() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        final List<KeyboardRow> keyboardRowList = new ArrayList<>();

        int i = 0;
        KeyboardRow menuClassificationDishes = new KeyboardRow();
        keyboardRowList.add(menuClassificationDishes);
        for (String type : productService.getAllProductType()) {
            if (i == 3) {
                menuClassificationDishes = new KeyboardRow();
                keyboardRowList.add(menuClassificationDishes);
                i = 0;
            }
            menuClassificationDishes.add(type);
            i++;
        }

        KeyboardRow toOrder = new KeyboardRow();
        toOrder.add("Контакты \uD83D\uDCDE");
        keyboardRowList.add(toOrder);

        KeyboardRow developerInfo = new KeyboardRow();
        developerInfo.add("О разработчике \uD83D\uDC68\u200D\uD83D\uDCBB");
        keyboardRowList.add(developerInfo);

        replyKeyboardMarkup.setKeyboard(keyboardRowList);

        return replyKeyboardMarkup;
    }
}
