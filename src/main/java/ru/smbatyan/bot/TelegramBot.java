package ru.smbatyan.bot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.smbatyan.bot.operators.CommandsOperator;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Component
public class TelegramBot extends TelegramLongPollingBot {

    private final Executor executor = Executors.newCachedThreadPool();
    private final CommandsOperator commandsOperator;

    public TelegramBot(@Value("${bot.token}") String botToken, CommandsOperator commandsOperator) {
        super(botToken);
        this.commandsOperator = commandsOperator;
    }

    @Override
    public void onUpdateReceived(Update update) {
         executor.execute(() -> commandsOperator.request(update, this));
    }

    @Override
    public String getBotUsername() {
        return "telegramBotMenu";
    }
}
