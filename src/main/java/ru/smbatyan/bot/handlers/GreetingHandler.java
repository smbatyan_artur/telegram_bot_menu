package ru.smbatyan.bot.handlers;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.smbatyan.bot.buttons.MenuButtons;
import ru.smbatyan.managers.ChatEntityManager;
import ru.smbatyan.managers.InformationToUserEntityManager;
import ru.smbatyan.models.Chat;
import ru.smbatyan.models.InformationToUser;

import java.util.Set;


@Component
public class GreetingHandler implements CommandHendler {

    private final MenuButtons menuButtons;
    private final InformationToUserEntityManager information;
    private final ChatEntityManager chatEntityManager;

    public GreetingHandler(MenuButtons menuButtons, InformationToUserEntityManager information, ChatEntityManager chatEntityManager) {
        this.menuButtons = menuButtons;
        this.information = information;
        this.chatEntityManager = chatEntityManager;
    }

    @Override
    public void processing(Update update, TelegramLongPollingBot telegramLongPollingBot) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setText(information.getInfo(InformationToUser.TypeInfo.GREETING));

        sendMessage.setReplyMarkup(menuButtons.getMenuButton());

        try {
            telegramLongPollingBot.executeAsync(sendMessage);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }

        chatEntityManager.addNewChat(Chat.builder().id(update.getMessage().getChatId()).build());
    }

    @Override
    public Set<String> getListCommandName() {
        return Set.of("/start");
    }
}
