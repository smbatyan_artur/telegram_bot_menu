package ru.smbatyan.bot.handlers;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Set;

public interface CommandHendler {
    void processing(Update update, TelegramLongPollingBot telegramLongPollingBot);
    Set<String> getListCommandName();
}
