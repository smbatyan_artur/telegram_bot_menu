package ru.smbatyan.bot.handlers;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.smbatyan.bot.buttons.MenuButtons;
import ru.smbatyan.managers.FileManager;
import ru.smbatyan.services.ProductService;
import ru.smbatyan.utils.NumberUtil;

import java.util.Set;

@Component
public class MenuHendler implements CommandHendler {

    private final ProductService productService;
    private final FileManager fileManager;
    private final MenuButtons menuButtons;

    public MenuHendler(ProductService productService, FileManager fileManager, MenuButtons menuButtons) {
        this.productService = productService;
        this.fileManager = fileManager;
        this.menuButtons = menuButtons;
    }


    @Override
    public void processing(Update update, TelegramLongPollingBot telegramLongPollingBot) {
        productService.getListByType(update.getMessage().getText().trim())
                .forEach(product -> {

                    SendPhoto sendPhoto = new SendPhoto();
                    sendPhoto.setChatId(update.getMessage().getChatId().toString());

                    sendPhoto.setPhoto(new InputFile(fileManager.getFile(product.getPhotoToken()), product.getPhotoToken()));
                    String text = "Название : " + product.getTitle() + "\n";
                    text += "Цена : " + NumberUtil.reduceTheFractionalPartToTwoUnits(product.getPrice()) + " ₽";
                    if (product.getDescription() != null) {
                        text += "\nОписание : " + product.getDescription();
                    }
                    sendPhoto.setCaption(text);

                    sendPhoto.setReplyMarkup(menuButtons.getMenuButton());

                    telegramLongPollingBot.executeAsync(sendPhoto);
                });

    }


    @Override
    public Set<String> getListCommandName() {
        return productService.getAllProductType();
    }
}
