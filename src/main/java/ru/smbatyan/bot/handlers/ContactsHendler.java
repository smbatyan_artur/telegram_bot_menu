package ru.smbatyan.bot.handlers;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.smbatyan.bot.buttons.MenuButtons;
import ru.smbatyan.managers.InformationToUserEntityManager;
import ru.smbatyan.models.InformationToUser;

import java.util.Set;

@Component
public class ContactsHendler implements CommandHendler{

    private final MenuButtons menuButtons;

    private final InformationToUserEntityManager information;

    public ContactsHendler(MenuButtons menuButtons, InformationToUserEntityManager information) {
        this.menuButtons = menuButtons;
        this.information = information;
    }

    @Override
    public void processing(Update update, TelegramLongPollingBot telegramLongPollingBot) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setText(information.getInfo(InformationToUser.TypeInfo.CONTACT_DETAILS));

        sendMessage.setReplyMarkup(menuButtons.getMenuButton());
        try {
            telegramLongPollingBot.executeAsync(sendMessage);
        } catch (TelegramApiException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Set<String> getListCommandName() {
        return Set.of("Контакты \uD83D\uDCDE");
    }
}

