package ru.smbatyan.bot.handlers;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.smbatyan.bot.buttons.MenuButtons;

import java.util.Set;


@Component
public class DefaultCommandHendler implements CommandHendler {

    private final MenuButtons menuButtons;

    public DefaultCommandHendler(MenuButtons menuButtons) {
        this.menuButtons = menuButtons;
    }

    @Override
    public void processing(Update update, TelegramLongPollingBot telegramLongPollingBot) {

        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setText("⤵\uFE0F");
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setReplyMarkup(menuButtons.getMenuButton());

        try {
            telegramLongPollingBot.executeAsync(sendMessage);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Set<String> getListCommandName() {
        return Set.of("Default");
    }
}
