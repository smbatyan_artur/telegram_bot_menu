package ru.smbatyan.bot.handlers;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.smbatyan.bot.buttons.MenuButtons;

import java.util.Set;

@Component
public class DeveloperInfoHendler implements CommandHendler {

    private final MenuButtons menuButtons;

    public DeveloperInfoHendler(MenuButtons menuButtons) {
        this.menuButtons = menuButtons;
    }

    @Override
    public void processing(Update update, TelegramLongPollingBot telegramLongPollingBot) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setText("Здравствуйте, меня зовут Артур. " +
                "Я являюсь разработчиком высоконагруженных систем, ботов и сайтов. " +
                "По вопросам сотрудничества, пожалуйста, свяжитесь со мной в Telegram. " +
                "Буду рад обсудить возможности сотрудничества. ⤵\uFE0F \n https://t.me/ArTuROoGaTtI");

        sendMessage.setReplyMarkup(menuButtons.getMenuButton());
        try {
            telegramLongPollingBot.executeAsync(sendMessage);
        } catch (TelegramApiException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Set<String> getListCommandName() {
        return Set.of("О разработчике \uD83D\uDC68\u200D\uD83D\uDCBB");
    }
}

