package ru.smbatyan.managers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.smbatyan.exceptions.FileWriteException;
import ru.smbatyan.utils.StringUtil;
import ru.smbatyan.utils.TokenUtil;

import java.io.*;

@Component
public class FileManagerImpl implements FileManager {

    @Value("${path.store.repository}")
    private String pathStoreRepository;

    @Override
    public InputStream getFile(String fileName) {
        try {
            return new FileInputStream(pathStoreRepository + "/" + fileName);
        } catch (IOException e) {
            return getClass().getResourceAsStream("/images/net_logo.jpg");
        }
    }

    @Override
    public String saveFile(InputStream inputStream, String type) {

        final File folder = new File(pathStoreRepository);
        if (!folder.exists()) folder.mkdir();

        final String originalFileName = TokenUtil.generateNewToken() + "." + StringUtil.getStringAfterElement(type, "/");

        try (FileOutputStream fileOutputStream = new FileOutputStream(pathStoreRepository + originalFileName)) {
            fileOutputStream.write(inputStream.readAllBytes());
            fileOutputStream.flush();

        } catch (IOException e) {
            throw new FileWriteException(e.getMessage());
        }
        return originalFileName;
    }


    @Override
    public void deleteFile(String fileName) {
        new File(pathStoreRepository + "/" + fileName).delete();
    }
}
