package ru.smbatyan.managers;

import ru.smbatyan.models.Chat;

public interface ChatEntityManager {
    Chat addNewChat(Chat chat);
}
