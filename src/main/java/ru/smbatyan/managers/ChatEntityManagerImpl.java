package ru.smbatyan.managers;

import org.springframework.stereotype.Component;
import ru.smbatyan.models.Chat;
import ru.smbatyan.repositories.ChatRepository;


@Component
public class ChatEntityManagerImpl implements ChatEntityManager {
    private final ChatRepository chatRepository;

    public ChatEntityManagerImpl(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }


    @Override
    public Chat addNewChat(Chat chat) {
        return chatRepository.findById(chat.getId()).orElseGet(() -> chatRepository.save(chat));
    }
}
