package ru.smbatyan.managers;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;
import ru.smbatyan.models.InformationToUser;

@Validated
public interface InformationToUserEntityManager {

    String update(@Valid @NotBlank String text, @NotNull InformationToUser.TypeInfo typeInfo);

    String getInfo(@Valid @NotNull InformationToUser.TypeInfo typeInfo);
}
