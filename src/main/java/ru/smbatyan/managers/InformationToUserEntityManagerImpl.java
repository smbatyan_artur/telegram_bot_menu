package ru.smbatyan.managers;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.smbatyan.models.InformationToUser;
import ru.smbatyan.repositories.InformationToUserRepository;

@Component
public class InformationToUserEntityManagerImpl implements InformationToUserEntityManager {

    private final InformationToUserRepository information;

    public InformationToUserEntityManagerImpl(InformationToUserRepository information) {
        this.information = information;
    }


    @PostConstruct
    private void setDefaultInfo() {
        if (information.findByTypeInfo(InformationToUser.TypeInfo.GREETING).isEmpty())
            information.saveAndFlush(InformationToUser.builder().typeInfo(InformationToUser.TypeInfo.GREETING).text("Welcome!").build());
        if (information.findByTypeInfo(InformationToUser.TypeInfo.CONTACT_DETAILS).isEmpty())
            information.saveAndFlush(InformationToUser.builder().typeInfo(InformationToUser.TypeInfo.CONTACT_DETAILS).text("Empty!").build());
    }


    @Transactional
    @Override
    public String update(String text, InformationToUser.TypeInfo typeInfo) {
        information.deleteAllByTypeInfo(typeInfo);
        information.saveAndFlush(InformationToUser.builder().text(text).typeInfo(typeInfo).build());
        return text;
    }

    @Override
    public String getInfo(InformationToUser.TypeInfo typeInfo) {
        String result = "";
        for (InformationToUser greetings : information.findByTypeInfo(typeInfo))
            result = greetings.getText();
        return result;
    }
}
