package ru.smbatyan.managers;

import java.io.InputStream;

public interface FileManager {
    InputStream getFile(String fileName);

    String saveFile(InputStream inputStream, String type);

    void deleteFile(String fileName);
}
