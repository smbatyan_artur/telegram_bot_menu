package ru.smbatyan.forms;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CreateProductForm {
    @NotBlank
    private String title;
    @NotNull
    @Min(1)
    private Double price;
    @NotNull
    private MultipartFile photo;
    private String description;
    @NotBlank
    private String type;
}
