package ru.smbatyan.forms;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SignInForm {
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
}
