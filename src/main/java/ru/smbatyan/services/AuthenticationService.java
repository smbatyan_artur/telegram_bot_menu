package ru.smbatyan.services;

import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import ru.smbatyan.dto.TokenInfoDro;
import ru.smbatyan.forms.SignInForm;

@Validated
public interface AuthenticationService {
    TokenInfoDro signIn(@Valid SignInForm signInForm);
}
