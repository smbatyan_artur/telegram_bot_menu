package ru.smbatyan.services;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.smbatyan.dto.ProductDto;
import ru.smbatyan.exceptions.ProductNotFoundException;
import ru.smbatyan.forms.CreateProductForm;
import ru.smbatyan.managers.FileManager;
import ru.smbatyan.models.Product;
import ru.smbatyan.repositories.ProductRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final FileManager fileManager;
    private final ModelMapper modelMapper;


    public ProductServiceImpl(ProductRepository productRepository, FileManager fileManager, ModelMapper modelMapper) {
        this.productRepository = productRepository;
        this.fileManager = fileManager;
        this.modelMapper = modelMapper;
    }


    @Override
    public ProductDto create(CreateProductForm createProductForm) {
        try {
            final String photoOriginalName = fileManager.saveFile(createProductForm.getPhoto().getInputStream(), createProductForm.getPhoto().getContentType());

            return modelMapper.map(productRepository.saveAndFlush(Product.builder()
                    .photoToken(photoOriginalName)
                    .title(createProductForm.getTitle())
                    .description(createProductForm.getDescription())
                    .type(createProductForm.getType())
                    .price(createProductForm.getPrice())
                    .isDelete(false)
                    .build()), ProductDto.class);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }


    @Override
    public Set<String> getAllProductType() {
        return productRepository.getAllProduct().stream().map(Product::getType).collect(Collectors.toSet());
    }

    @Override
    public Set<ProductDto> getAllProdict() {
        return productRepository.getAllProduct()
                .stream()
                .map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toSet());
    }


    @Override
    public ProductDto delete(Long productId) {
        final Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isEmpty())
            throw new ProductNotFoundException("Product by id " + productId + " not found");
        else {
            final Product product = optionalProduct.get();
            fileManager.deleteFile(product.getPhotoToken());
            product.setIsDelete(true);
            return modelMapper.map(productRepository.saveAndFlush(product), ProductDto.class);
        }
    }

    @Override
    public List<ProductDto> search(String title) {
        return productRepository.search(title)
                .stream()
                .map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Set<ProductDto> getListByType(String type) {
        return productRepository.getListByType(type)
                .stream()
                .map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toSet());
    }


    @Override
    public ProductDto updatePrice(Long productId, Double price) {
        final Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isEmpty())
            throw new ProductNotFoundException("Product by id " + productId + " not found");
        else {
            final Product product = optionalProduct.get();
            product.setPrice(price);
            return modelMapper.map(productRepository.saveAndFlush(product), ProductDto.class);
        }
    }
}
