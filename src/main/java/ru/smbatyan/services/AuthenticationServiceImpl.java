package ru.smbatyan.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.smbatyan.dto.TokenInfoDro;
import ru.smbatyan.exceptions.UserNotFoundException;
import ru.smbatyan.forms.SignInForm;

import java.time.LocalDateTime;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final Algorithm algorithm;
    @Value("${admin.username}")
    private String adminUsername;
    @Value("${admin.password}")
    private String adminPassword;

    public AuthenticationServiceImpl(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    @Override
    public TokenInfoDro signIn(SignInForm signInForm) {
        if (signInForm.getPassword().equals(adminPassword) && signInForm.getUsername().equals(adminUsername)) {
            String token = JWT.create()
                    .withClaim("username", signInForm.getUsername())
                    .withClaim("createTokenDate", LocalDateTime.now().toString())
                    .withClaim("role","ROLE_ADMIN")
                    .sign(algorithm);
            return TokenInfoDro.builder().token(token).type("bearer").build();
        }
        throw new UserNotFoundException("The user with username -> " + signInForm.getUsername() + " <- was not found");
    }
}
