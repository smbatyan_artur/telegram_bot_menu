package ru.smbatyan.services;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;
import ru.smbatyan.dto.ProductDto;
import ru.smbatyan.forms.CreateProductForm;

import java.util.List;
import java.util.Set;

@Validated
public interface ProductService {

    ProductDto create(@Valid CreateProductForm createProductForm);

    Set<String> getAllProductType();

    Set<ProductDto> getAllProdict();

    ProductDto delete(@Valid @NotNull Long productId);

    List<ProductDto> search(@Valid @NotBlank String title);

    Set<ProductDto> getListByType(@Valid @NotBlank String type);

    ProductDto updatePrice(@Valid @NotNull Long productId, @NotNull @Min(1) Double price);
}
