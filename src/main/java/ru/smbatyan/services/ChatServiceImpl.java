package ru.smbatyan.services;

import jakarta.annotation.PostConstruct;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.smbatyan.repositories.ChatRepository;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.LockSupport;


@Service
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;
    private final TelegramLongPollingBot telegramLongPollingBot;
    private final Executor executor = Executors.newFixedThreadPool(1);
    private final Semaphore semaphore = new Semaphore(30);

    public ChatServiceImpl(ChatRepository chatRepository, TelegramLongPollingBot telegramLongPollingBot) {
        this.chatRepository = chatRepository;
        this.telegramLongPollingBot = telegramLongPollingBot;
    }

    @Override
    public Integer getQuantityChats() {
        return chatRepository.findAll().size();
    }

    private void sendMessage(String message, Long chatId) {
        try {
            semaphore.acquire();
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId);
            sendMessage.setText(message);
            try {
                telegramLongPollingBot.executeAsync(sendMessage);
            } catch (TelegramApiException e) {
                throw new IllegalArgumentException(e);
            }
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @PostConstruct
    private void resetSemaphore() {
        executor.execute(() -> {
            semaphore.release(30);
            LockSupport.parkNanos(60000000000L);
        });
    }


    @Async
    @Override
    public void mailing(String message) {
        chatRepository.findAll().forEach(chat -> sendMessage(message, chat.getId()));
    }
}
