package ru.smbatyan.utils;

public class StringUtil {

    public static String getStringAfterElement(String str, String element) {
        final char[] charsStringType = str.toCharArray();
        final StringBuilder result = new StringBuilder();
        for (int i = str.indexOf(element) + 1; i < charsStringType.length; i++)
            result.append(charsStringType[i]);
        return result.toString();
    }

    public static String getString(int start, String str) {
        char[] c = str.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = start; i < c.length; i++) {
            result.append(c[i]);
        }
        return result.toString();
    }
}
