package ru.smbatyan.utils;


import java.text.NumberFormat;
import java.text.ParseException;

public class NumberUtil {

    public static double reduceTheFractionalPartToTwoUnits(double d){
        try {
            return NumberFormat.getNumberInstance().parse(String.format("%.2f", d)).doubleValue();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
