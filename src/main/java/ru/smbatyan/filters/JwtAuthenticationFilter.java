package ru.smbatyan.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.filter.GenericFilterBean;
import ru.smbatyan.security.JwtAuthenticationProvider;

import java.io.IOException;

public class JwtAuthenticationFilter extends GenericFilterBean {

    private final JwtAuthenticationProvider jwtProvider;

    public JwtAuthenticationFilter(JwtAuthenticationProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String token = request.getHeader("Authorization");


        if (token != null)
            jwtProvider.authenticationByToken(token);

        filterChain.doFilter(request, response);
    }
}