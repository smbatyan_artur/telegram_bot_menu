package ru.smbatyan.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "information_to_user")
public class InformationToUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text", length = 1000, nullable = false)
    private String text;

    @Column(name = "type_info", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TypeInfo typeInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InformationToUser that = (InformationToUser) o;
        return Objects.equals(id, that.id) && Objects.equals(text, that.text) && typeInfo == that.typeInfo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, typeInfo);
    }

    public enum TypeInfo {
        GREETING, CONTACT_DETAILS
    }
}
