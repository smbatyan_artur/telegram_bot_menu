package ru.smbatyan.models;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "photo_token", nullable = false, unique = true)
    private String photoToken;

    @Nullable
    @Column(name = "description")
    private String description;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Objects.equals(title, product.title) && Objects.equals(price, product.price) && Objects.equals(photoToken, product.photoToken) && Objects.equals(description, product.description) && Objects.equals(type, product.type) && Objects.equals(isDelete, product.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, price, photoToken, description, type, isDelete);
    }
}
