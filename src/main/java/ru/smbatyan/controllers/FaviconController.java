package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FaviconController {

    @Hidden
    @GetMapping("favicon.ico")
    @ResponseBody
    void returnNoFavicon() {}

}
