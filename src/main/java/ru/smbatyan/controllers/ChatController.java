package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.smbatyan.services.ChatService;

@SecurityRequirement(name = "JWT")
@Tag(name = "Управление чатами")
@RestController
@RequestMapping("/chat")
public class ChatController {

    private final ChatService chatService;

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @GetMapping("/quantity")
    public ResponseEntity<Integer> getNumberChats() {
        return ResponseEntity.ok(chatService.getQuantityChats());
    }


    @PostMapping("/mailing")
    public ResponseEntity<Void> mailing(String message) {
        chatService.mailing(message);
        return ResponseEntity.ok().build();
    }
}
