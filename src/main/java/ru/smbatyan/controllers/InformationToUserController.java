package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.smbatyan.managers.InformationToUserEntityManager;
import ru.smbatyan.models.InformationToUser;

@SecurityRequirement(name = "JWT")
@Tag(name = "Управление информацией для пользователей")
@RestController
@RequestMapping("/information_to_user")
public class InformationToUserController {

    private final InformationToUserEntityManager information;

    public InformationToUserController(InformationToUserEntityManager information) {
        this.information = information;
    }

    @PostMapping("/update")
    public ResponseEntity<String> update(@RequestParam(name = "text") String text, @RequestParam(name = "typeInfo") InformationToUser.TypeInfo typeInfo) {
        return ResponseEntity.ok(information.update(text, typeInfo));
    }
}
