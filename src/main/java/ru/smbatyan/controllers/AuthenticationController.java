package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.smbatyan.dto.TokenInfoDro;
import ru.smbatyan.forms.SignInForm;
import ru.smbatyan.services.AuthenticationService;

@Tag(name = "Аутентификация")
@RestController
@RequestMapping("/")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/signIn")
    public ResponseEntity<TokenInfoDro> signIn(@RequestBody SignInForm signInForm) {
        return ResponseEntity.ok(authenticationService.signIn(signInForm));
    }
}

