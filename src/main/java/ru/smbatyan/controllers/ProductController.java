package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.smbatyan.dto.ProductDto;
import ru.smbatyan.forms.CreateProductForm;
import ru.smbatyan.services.ProductService;

import java.util.List;
import java.util.Set;


@SecurityRequirement(name = "JWT")
@Tag(name = "Управление продуктами")
@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ProductDto> create(@ModelAttribute CreateProductForm createProductForm) {
        return ResponseEntity.ok(productService.create(createProductForm));
    }


    @GetMapping("/types")
    public ResponseEntity<Set<String>> getAllProductType() {
        return ResponseEntity.ok(productService.getAllProductType());
    }


    @GetMapping("/all")
    public ResponseEntity<Set<ProductDto>> getAllProduct() {
        return ResponseEntity.ok(productService.getAllProdict());
    }


    @DeleteMapping("/")
    public ResponseEntity<ProductDto> delete(@RequestParam("product_id") Long productId) {
        return ResponseEntity.ok(productService.delete(productId));
    }


    @GetMapping("/search")
    public ResponseEntity<List<ProductDto>> search(@RequestParam(name = "title") String title) {
        return ResponseEntity.ok(productService.search(title));
    }


    @GetMapping("/list/by/type")
    public ResponseEntity<Set<ProductDto>> getListByType(@RequestParam(name = "type") String type) {
        return ResponseEntity.ok(productService.getListByType(type));
    }


    @PatchMapping("/update/price")
    public ResponseEntity<ProductDto> updatePrice(@RequestParam(name = "product_id") Long productId, @RequestParam(name = "price") Double price) {
        return ResponseEntity.ok(productService.updatePrice(productId, price));
    }
}
