package ru.smbatyan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.smbatyan.models.Chat;

public interface ChatRepository extends JpaRepository<Chat, Long> {
}
