package ru.smbatyan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.smbatyan.models.InformationToUser;

import java.util.List;

public interface InformationToUserRepository extends JpaRepository<InformationToUser, Long> {
    List<InformationToUser> findByTypeInfo(InformationToUser.TypeInfo typeInfo);

    void deleteAllByTypeInfo(InformationToUser.TypeInfo typeInfo);
}
