package ru.smbatyan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.smbatyan.models.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT p FROM Product p WHERE p.isDelete = false")
    List<Product> getAllProduct();

    @Query("SELECT p FROM Product p WHERE UPPER(p.title) ILIKE CONCAT(:title, '%') AND p.isDelete=false")
    List<Product> search(@Param(value = "title") String title);

    @Query("SELECT p FROM Product p WHERE p.isDelete = false AND p.type =:type")
    List<Product> getListByType(@Param(value = "type") String type);
}
